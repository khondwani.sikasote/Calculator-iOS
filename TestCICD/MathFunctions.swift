//
//  MathFunctions.swift
//  TestCICD
//
//  Created by Khondwani Sikasote on 2022/07/25.
//

import Foundation

class MathFunctions {
    func addNumbers(val1: Double, val2: Double) -> Double {
        return val1 + val2
    }
    
    func divideNumbers(val1: Double, val2: Double) -> Double {
        return val1 / val2
    }
    
    func multipleNumbers(val1: Double, val2: Double) -> Double {
        return val1 * val2
    }
    
    
    func subtractNumbers(val1:Double,val2:Double) -> Double {
        return val1 - val2
    }
}
