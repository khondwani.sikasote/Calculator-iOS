//
//  TestCICDTests.swift
//  TestCICDTests
//
//  Created by Khondwani Sikasote on 2022/07/25.
//

import XCTest
@testable import TestCICD // allows us to test items from this file/framework

class TestCICDTests: XCTestCase {
    func testAdd() {
        let calculate = MathFunctions()
        let result = calculate.addNumbers(val1: 1, val2: 1)
        XCTAssertEqual(result, 2)
    }
    func testDivide() {
        let calculate = MathFunctions()
        let result = calculate.divideNumbers(val1: 1, val2: 1)
        XCTAssertEqual(result, 1.0)
    }
    func testMultiple() {
        let calculate = MathFunctions()
        let result = calculate.multipleNumbers(val1: 1, val2: 1)
        XCTAssertEqual(result, 1)
    }
    
    func testSubtraction() {
        let calculate = MathFunctions()
        let result = calculate.subtractNumbers(val1: 1, val2: 1)
        XCTAssertEqual(result, 0.0)
    }
}
